# io-scheduler-udev-rules

Udev rules to allow easier customization of kernel I/O schedulers

Inspired on https://github.com/openSUSE/system-tuning-SUSE/blob/master/60-io-scheduler.rules

Allows the users to define their preferences either editing /etc/default/io-scheduler
or using the traditional elevator=xxx kernel command line parameter.

When user doesn't specify anything, it relies on defaults set by udev rule.
